# MacOS experience

MacOS-like keybindings for windows and linux.

Unlike [kinto.sh](https://kinto.sh) it initially designed non-destructive way, avoiding key conflicts as much as possible.

The goal is to use the best keys from all worlds and keep it consistent.  So, it use MacOS core ideas, like Command-letters, Emacs-like bindings and Command-Shift-braces, but it is not the focus to be 100% MacOS compatible.  MacOS itself is not perfect in it.  But core ideas are good and worth to be implemented.

## Setup

### Windows 11

1. setup visuals (panel to the top):
  - `Super+R`, `regedit`, `Enter`
  - navigate to `HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\StuckRects3`
  - open the Settings key for Editing by double-clicking it
  - change the fifth value in the second row from \"03\" to \"01.\" You can do this by deleting the 03 and then typing 01. Click Ok
  - restart `explorer.exe` or just relogin or reboot
2. install autohotkey v2 (no admin rights required):
```powershell
iwr -useb get.scoop.sh | iex
scoop install git
scoop bucket add extras
scoop install autohotkey
```
3. download [w11.ahk](w11.ahk), put it into startup (`Super+R`, `shell:startup`, `Enter`) and/or run

### Pop OS (Xremap version, tested on Pop OS 22.04)

1. Download [Xremap](https://github.com/k0kubun/xremap) X11 version and enable uinput rule:
``` bash
xremap_url=$(curl -sL https://api.github.com/repos/k0kubun/xremap/releases/latest | grep x86_64-x11.zip | grep browser | cut -d'"' -f4)
xremap_name=$(echo $xremap_url | cut -d'/' -f9)

wget $xremap_url

mkdir -p ~/.local/bin/
7z x $xremap_name -O./.local/bin/
rm $xremap_name

sudo gpasswd -a $USER input
echo 'KERNEL=="uinput", GROUP="input", TAG+="uaccess"' | sudo tee /etc/udev/rules.d/input.rules
```
2. Put [pop.yaml](pop.yaml) to convenient place
3. Run it like this:
``` bash
xremap pop.yaml
```
4. Add full command to autorun via Gnome "Startup Applications", like this:
``` bash
/home/user/.local/bin/xremap --watch /home/user/Documents/macos_experience/pop.yaml
```
