;----------------------------------------------------------------------
;; GLOBALS

;spotlight
!Space::#s
;lock screen
^!q::DllCall("LockWorkStation")
;close current window or app
#q::!F4
;hide current window
#h::WinMinimize "A"
;hide all windows except curren t
!#h::
{
  active_id := WinGetID("A")
  WinMinimizeAll
  WinActivate active_id
}
;flameshot hotkey, the same buttons as with macos flameshot defaults
!+x::PrintScreen

;----------------------------------------------------------------------
;; GROUPS

;; General
; system
GroupAdd "system", "ahk_class #32770" ; Win+R
GroupAdd "system", "ahk_class ApplicationFrameWindow ahk_exe ApplicationFrameHost.exe" ; Win+S, Win11 Clock app and settings window
GroupAdd "system", "ahk_class Windows.UI.Core.CoreWindow ahk_exe SearchHost.exe" ; Win+S, Spotlight
GroupAdd "system", "ahk_class XamlExplorerHostIslandWindow ahk_exe explorer.exe" ; Win+Tab, Mission Control
GroupAdd "system", "ahk_class Shell_TrayWnd ahk_exe explorer.exe" ; panel
GroupAdd "system", "ahk_exe CredentialUIBroker.exe" ; credentials request
GroupAdd "system", "ahk_class WinUIDesktopWin32WindowClass" ; used at least by PowerToys
GroupAdd "system", "ahk_class #32770" ; save dialog
GroupAdd "general", "ahk_group system"
; browsers
GroupAdd "chromes", "ahk_exe brave.exe"
GroupAdd "chromes", "ahk_exe chrome.exe"
GroupAdd "chromes", "ahk_exe msedge.exe"
GroupAdd "firefoxes", "ahk_exe firefox.exe"
GroupAdd "browsers", "ahk_group firefoxes"
GroupAdd "browsers", "ahk_group chromes"
GroupAdd "general", "ahk_group browsers"
; vscodes
GroupAdd "vscodes", "ahk_exe VSCodium.exe"
GroupAdd "vscodes", "ahk_exe Code.exe"
GroupAdd "general", "ahk_group vscodes"
; office
GroupAdd "office", "ahk_exe outlook.exe"
GroupAdd "office", "ahk_exe excel.exe"
GroupAdd "office", "ahk_exe winword.exe"
GroupAdd "office", "ahk_exe powerpnt.exe"
GroupAdd "office", "ahk_exe teams.exe"
GroupAdd "office", "ahk_exe onenote.exe"
GroupAdd "office", "ahk_exe onedrive.exe"
GroupAdd "general", "ahk_group office"
; other
GroupAdd "general", "ahk_exe KeePass.exe"
GroupAdd "general", "ahk_exe KeePassXC.exe"
GroupAdd "general", "ahk_exe Postman.exe"
GroupAdd "general", "ahk_exe webexhost.exe"
GroupAdd "general", "ahk_exe atmgr.exe"
GroupAdd "general", "ahk_exe ciscocollabhost.exe"
GroupAdd "general", "ahk_exe wmlhost.exe"
GroupAdd "general", "ahk_class Notepad ahk_exe notepad.exe"
GroupAdd "general", "ahk_exe One Photo Viewer.exe"
GroupAdd "general", "ahk_exe SCNotification.exe"
GroupAdd "general", "ahk_exe flameshot.exe"
GroupAdd "general", "ahk_exe PowerToys.PowerLauncher.exe"

;; Terminals
GroupAdd "terminal", "ahk_exe WindowsTerminal.exe"

;; File managers
GroupAdd "explorer", "ahk_class CabinetWClass ahk_exe explorer.exe" ; file browser
GroupAdd "explorer", "ahk_class WorkerW ahk_exe explorer.exe" ; desktop

;----------------------------------------------------------------------
;; CONDITIONAL MAPPING

#HotIf WinActive("ahk_group general")
{
  !1::^1
  !2::^2
  !3::^3
  !4::^4
  !5::^5
  !6::^6
  !7::^7
  !8::^8
  !9::^9
  !0::^0

  !a::^a
  !b::^b
  !c::^c
  !d::^d
  !e::^e
  !f::^f
  !g::^g
  !h::^h
  !i::^i
  !j::^j
  !k::^k
  !l::^l
  !m::^m
  !n::^n
  !o::^o
  !p::^p
  !q::^q
  !r::^r
  !s::^s
  !t::^t
  !u::^u
  !v::^v
  ^!v::^!v
  !w::^w
  !x::^x
  !y::^y
  !z::^z
  !+z::^y

  !enter::^enter

  ; bug: may trigger alt-tab language layout switch if not disabled
  !+sc01B::^pgdn
  !+sc01A::^pgup
  !sc01B::!right
  !sc01A::!left

  ^f::right
  ^b::left
  ^p::up
  ^n::down
  ^a::home
  ^e::end
  ^k::Send "{Shift Down}{End}{Shift Up}{Del}"
  ^d::del

  #backspace::^backspace
  #del::^del
  ; uncomment row below if You ok to broke "show desktop"
  ; #d::^del
  !backspace::Send "{Shift Down}{Home}{Shift Up}{Backspace}"
  !del::Send "{Shift Down}{End}{Shift Up}{Del}"

  ; uncomment rows below if You ok to broke left-right snapping
  ; #right::^right
  ; #left::^left
  !left::home
  !right::end
  !up::^home
  !down::^end
}


#HotIf WinActive("ahk_group chromes")
{
  !,::Send "{Ctrl Down}{t}{Ctrl Up}chrome://settings{Enter}"
}

#HotIf WinActive("ahk_group firefoxes")
{
  !,::Send "{Ctrl Down}{t}{Ctrl Up}about:preferences{Enter}"
}

; #HotIf WinActive("ahk_group vscodes")
; {
;   ;move line
;   #up::!up
;   #down::!down
;   ;copy line
;   #+up::!+up
;   #+down::!+down
;   ;multiple cursors
;   #lbutton::!lbutton
;   #!up::^!up
;   #!down::^!down
;   ;sublime-like multiple cursors
;   ^+up::^!up
;   ^+down::^!down
;   ;open settings
;   !,::^,
;   ;search
;   !f::^f
;   !+h::^+h
;   ;view source control
;   !+g::Send "{Ctrl Down}{Shift Down}{g}{Shift Up}{Ctrl Up}{g}"
;   ;fold/unfold
;   !#sc01A::^+sc01A
;   !#sc01B::^+sc01B
;   ;toggle comment
;   !/::^/
;   #+a::!+a
;   ;word wrap
;   #z::!z
;   ;peek definition
;   #F12::!F12
;   ;quick fix
;   !.::^.
;   ;format
;   #+f::!+f
;   ;trigger parameter hints
;   !+space::^+space
;   ;save all
;   !#s::Send "{Ctrl Down}{k}{Ctrl Up}{s}"
;   ;command palette
;   !+p::^+p
; }

#HotIf WinActive("ahk_group terminal")
{
  !,::^,
  !+,::^+,

  !t::^+t
  !w::^+w
  !n::^+n
  !c::^+c
  !v::^+v
  !m::^+m
  !p::^+p
  !a::^+a
  !+f::^+f

  !+sc01B::^tab
  !+sc01A::^+tab
  ^pgdn::^tab
  ^pgup::^+tab

  !f::^right
  !b::^left
  ^f::right
  ^b::left
  ^p::up
  ^n::down
  ^a::home
  ^e::end
  ^k::Send "{Shift Down}{End}{Shift Up}{Del}"
  ^u::Send "{Shift Down}{Home}{Shift Up}{Backspace}"
  ^d::del

  #backspace::^backspace
  #del::^del
  !backspace::Send "{Shift Down}{Home}{Shift Up}{Backspace}"
  !del::Send "{Shift Down}{End}{Shift Up}{Del}"

  ; uncomment rows below if You ok to broke left-right snapping
  ; #right::^right
  ; #left::^left
  !left::home
  !right::end
}

#HotIf WinActive("ahk_group explorer")
{
  !down::enter
  !o::enter
  !i::!enter

  !z::^z
  !x::^x
  !c::^c
  !v::^v
  !a::^a
  !n::^n
  !l::^l
  !t::^t
  !w::^w

  !1::^+2
  !2::^+6
  !3::^+5
  !4::^+1

  ^f::right
  ^b::left
  ^p::up
  ^n::down
  ^a::home
  ^e::end
  ^d::del
  !backspace::del
}

;----------------------------------------------------------------------
